import { createBotRequest, addNewMenu, addNewButton } from '../requests/botRequests'
export function createBot(platform, name, description, token)
{
	return createBotRequest(17, name, description, token);
}

// export function addMenu(title, description, image_url, button_text)
export function addMenu(menu)
{
	return addNewMenu(menu);
	// return addNewMenu(title,description,image_url,button_text);
}

export function addButton(button)
{
	return addNewButton(button);
	// return addNewMenu(title,description,image_url,button_text);
}