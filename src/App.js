import React, { Component } from 'react'
import './includes/css/styles.css'
import { connect } from 'react-redux'
import { login } from './actions/loginActions'
import { browserHistory } from 'react-router'
import { Grid, Row, Col } from 'react-bootstrap'
import { Form, FormGroup, FormControl } from 'react-bootstrap'
import { ControlLabel, Button, Panel } from 'react-bootstrap'

// let loaderClass = "";
let errorMessage = "";

const mapStateToProps = (state) => {
  return {
    login: state.login,
  }
}


const divStyle = {
  'marginTop': '40px',
  'marginBottom': '40px',
}


class LoginPage extends Component {

  componentWillUpdate(nextProps, nextState) {

    if(nextProps.login.fetching === true)
    {
      // loaderClass = "ui active transition visible dimmer";
      errorMessage = ""
    }

    if(nextProps.login.fetched === true && nextProps.login.access_token != null)
    {
      // loaderClass = "";
      errorMessage = "";
      browserHistory.push("/main")
    }

    if(nextProps.login.error != null)
    {
      // loaderClass = "";
      errorMessage = "Username or Password is incorrect. Try again."
    }
  }

  login(){
    let username = this.state.usernameInput
    let password = this.state.passwordInput
    this.props.dispatch(login(username, password))
  }

  handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      this.login();
    }
  }


  handleUsernameChange = (event) => {
      this.setState({ usernameInput: event.target.value }, null)
  }

  handlePasswordChange = (event) => {
      this.setState({ passwordInput: event.target.value }, null)
  }

  render() {
    return (
      <Grid>
        <Row style={divStyle}>
          <Col md={6} mdOffset={3} >
            <Panel header="Chatbot System" bsStyle="primary">
            <span className="loadingPageErrorMessage">{errorMessage}</span>
              <Form horizontal>
                <FormGroup controlId="formHorizontalEmail">
                  <Col componentClass={ControlLabel} sm={2}>
                    Username:
                  </Col>
                  <Col sm={10}>
                    <FormControl type="text" placeholder="Username" onChange={this.handleUsernameChange} />
                  </Col>
                </FormGroup>

                <FormGroup controlId="formHorizontalPassword">
                  <Col componentClass={ControlLabel} sm={2}>
                    Password:
                  </Col>
                  <Col sm={10}>
                    <FormControl type="password" placeholder="Password" onChange={this.handlePasswordChange} onKeyPress={this.handleKeyPress.bind(this)}/>
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col smOffset={2} sm={10}>
                    <Button type="button" bsStyle="success" bsSize="small" onClick={this.login.bind(this)}>
                      Sign in
                    </Button>
                  </Col>
                </FormGroup>
              </Form>
            </Panel>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default connect(mapStateToProps)(LoginPage)