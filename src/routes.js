import React from 'react';
import { browserHistory, Router, Route, IndexRoute } from 'react-router';
import BotCreationMain from './components/botCreationMain';
import Main from './components/main';
import CreateBotForm from './components/body/createBotForm';
import EditBotForm from './components/body/editBotForm';
import BotFlowEditorMain from './components/botFlowEditorMain';
import BotFlowEditorMenu from './components/body/botFlowEditorMenu';
import BotFlowEditorOrderCount from './components/body/botFlowEditorOrderCount';
import BotFlowEditorCart from './components/body/botFlowEditorCart';
import BotFlowEditorPickupOrDelivery from './components/body/botFlowEditorPickupOrDelivery';
import BotFlowEditorCheckout from './components/body/botFlowEditorCheckout';
import App from './App';
import { Provider } from 'react-redux';
import store from './store';


const Routes = () => {
	return (
		<Provider store={store} >
			<Router history={browserHistory}>
				<Route path="/" component={App} />
				<Route path="/main" component={Main}/>
				<Route path="/bot" component={BotCreationMain}>
					<IndexRoute component={CreateBotForm}/>
					<Route path="/bot/create_bot" component={CreateBotForm}/>
					<Route path="/bot/edit_bot" component={EditBotForm}/>
				</Route>
				<Route path="bot/bot_flow_editor" component={BotFlowEditorMain}>
					<IndexRoute component={BotFlowEditorMenu} />
				    <Route path="/bot/bot_flow_editor/menu" component={BotFlowEditorMenu} />
				    <Route path="/bot/bot_flow_editor/order_count" component={BotFlowEditorOrderCount} />
				    <Route path="/bot/bot_flow_editor/cart" component={BotFlowEditorCart} />
				    <Route path="/bot/bot_flow_editor/pickup_or_delivery" component={BotFlowEditorPickupOrDelivery} />
				    <Route path="/bot/bot_flow_editor/checkout" component={BotFlowEditorCheckout} />
				</Route>
			</Router>
		</Provider>
	)
}

export default  Routes;