import React, { Component } from 'react';
import TopBarMenu from './commons/header/index';
import BotFlowEditorSideBarMenu from './commons/sidebar/botFlowEditorSidebar';

class BotFlowEditorMain extends Component{
  render(){
    if(this.props.location.pathname === "/bot/bot_flow_editor/menu" || this.props.location.pathname === "/bot/bot_flow_editor"){
      sessionStorage.setItem("botFlowEditorLink1","botFlowEditorLink1")
      sessionStorage.setItem("botFlowEditorLink2","")
      sessionStorage.setItem("botFlowEditorLink3","")
      sessionStorage.setItem("botFlowEditorLink4","")
      sessionStorage.setItem("botFlowEditorLink5","")
    }else if(this.props.location.pathname === "/bot/bot_flow_editor/order_count"){
      sessionStorage.setItem("botFlowEditorLink1","")
      sessionStorage.setItem("botFlowEditorLink2","botFlowEditorLink2")
      sessionStorage.setItem("botFlowEditorLink3","")
      sessionStorage.setItem("botFlowEditorLink4","")
      sessionStorage.setItem("botFlowEditorLink5","")
    }else if(this.props.location.pathname === "/bot/bot_flow_editor/cart"){
      sessionStorage.setItem("botFlowEditorLink1","")
      sessionStorage.setItem("botFlowEditorLink2","")
      sessionStorage.setItem("botFlowEditorLink3","botFlowEditorLink3")
      sessionStorage.setItem("botFlowEditorLink4","")
      sessionStorage.setItem("botFlowEditorLink5","")
    }else if(this.props.location.pathname === "/bot/bot_flow_editor/pickup_or_delivery"){
      sessionStorage.setItem("botFlowEditorLink1","")
      sessionStorage.setItem("botFlowEditorLink2","")
      sessionStorage.setItem("botFlowEditorLink3","")
      sessionStorage.setItem("botFlowEditorLink4","botFlowEditorLink4")
      sessionStorage.setItem("botFlowEditorLink5","")
    }
    else{
      sessionStorage.setItem("botFlowEditorLink1","")
      sessionStorage.setItem("botFlowEditorLink2","")
      sessionStorage.setItem("botFlowEditorLink3","")
      sessionStorage.setItem("botFlowEditorLink4","")
      sessionStorage.setItem("botFlowEditorLink5","botFlowEditorLink5")
    }
    return(
      <div>
        <TopBarMenu />
        <BotFlowEditorSideBarMenu />
        <div className="main">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default BotFlowEditorMain;
