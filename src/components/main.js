import React, { Component } from 'react'
import { Link } from 'react-router'
import TopBarMenu from './commons/header/index'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { logout } from '../actions/loginActions'
import { Grid, Row, Col, Table } from 'react-bootstrap'

const mapStateToProps = (state) => 
{
  return {
    login: state.login,
  }
}

const divStyle = {
  'marginTop': '40px',
  'marginBottom': '40px',
}


class Main extends Component {

	componentWillMount(){
		if(typeof(Storage) !== "undefined") {
        	if(sessionStorage.getItem("token") === null || this.props.login.access_token === null)
			{
				window.localStorage.clear()
				this.props.dispatch(logout(sessionStorage.getItem("username"), sessionStorage.getItem("token")))
				console.log("errorr!!! ~~~" + sessionStorage.getItem("token") + "~~~" + this.props.login.access_token)
				browserHistory.push("/");
			}
        } 
        else 
        {
            if(this.props.login.access_token === null)
            {
            	this.props.dispatch(logout(sessionStorage.getItem("username"), sessionStorage.getItem("token")))
				console.log("errorr!!! ~~~" + sessionStorage.getItem("token") + "~~~" + this.props.login.access_token)
				browserHistory.push("/")
            }
        }
	}

 	render() {
    	return (
	      	<div>
		    	<TopBarMenu />
			    	<Grid>
			    		<Row style={divStyle}>
          				  	<Col md={10} mdOffset={1} >
					    	 	<Table striped bordered>
								    <tbody>
								      <tr>
								        <td className="mainMenuBackground">Bot Creation</td>
								        <td ><Link to="/bot"><button type="button" className="btn btn-primary"> Create Bots</button></Link></td>
								        <td><button type="button" className="btn btn-primary"> Edit Bots</button></td>
								      </tr>
								      <tr>
								        <td className="mainMenuBackground">Bot Administration</td>
								        <td><button type="button" className="btn btn-primary"> Manage Bot Settings</button></td>
								        <td><button type="button" className="btn btn-primary"> Administer Bot Settings</button></td>
								      </tr>
								      <tr>
								        <td className="mainMenuBackground">Business Process Manager</td>
								        <td><button type="button" className="btn btn-primary"> Order Management</button></td>
								        <td><button type="button" className="btn btn-primary"> FAQ Mgt, Survey Mgt and etc.</button></td>
								      </tr>
								    </tbody>
							  	</Table>
							</Col>
						</Row>
					</Grid>
			</div>
    );
  }
}
export default connect(mapStateToProps)(Main);

