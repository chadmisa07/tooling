import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createBot } from '../../actions/botActions'
import store from '../../store';
import '../../includes/css/styles.css';
import { browserHistory } from 'react-router';
import { Grid, Row, Col } from 'react-bootstrap'
import { Form, FormGroup, FormControl } from 'react-bootstrap'
import { ControlLabel, Button } from 'react-bootstrap'

// let loaderClass = "";
let errorMessage = "";

const mapStateToProps = (state) => 
{
  return {
    createBot: state.createBot,
  }
}

const divStyle = {
  'marginTop': '40px',
  'marginLeft': '-200px',
  'float':'left',
  'width': '500px',
}

class CreateBotForm extends Component {

  componentWillUpdate(nextProps, nextState){
    
    if(nextProps.createBot.creating === true)
    {
       // loaderClass = "ui active transition visible dimmer";
       errorMessage = "";
    }

    if(nextProps.createBot.success === true && nextProps.createBot.creating === false)
    {
      // loaderClass = "";
      errorMessage = "";
      browserHistory.push('/bot/edit_bot');
    }

    if(nextProps.createBot.error != null)
    {
      errorMessage = "Sorry, an error occured. Please try again.";
      // loaderClass = "";
    }
  }

  create(){
    const storeData = store.getState();
    const token = storeData.login.access_token;
    let name = this.state.botNameInput;
    let description = this.state.botDescriptionInput;
    this.props.dispatch(createBot(17, name, description, token))
  }

  handleBotNameChange = (event) => {
    this.setState({ botNameInput: event.target.value }, null)
  }

  handleBotDescriptionChange = (event) => {
    this.setState({ botDescriptionInput: event.target.value }, null)
  }
  render() {
    return (
      <Grid>
        <Row style={divStyle}>
          <Col md={12} >
            <span className="loadingPageErrorMessage">{errorMessage}</span>
              <Form horizontal>
               <FormGroup controlId="formControlsSelect">
                  <ControlLabel>Choose a Platform</ControlLabel>
                  <FormControl componentClass="select" placeholder="select">
                    <option value=""></option>
                    <option value="fb">Facebook</option>
                    <option value="imessage">iMessage</option>
                    <option value="whatsapp">WhatsApp</option>
                    <option value="viber">Viber</option>
                  </FormControl>
                </FormGroup>
                  
                <FormGroup controlId="formHorizontalEmail">
                  <ControlLabel>Bot Name</ControlLabel>
                  <FormControl type="text" placeholder="Bot Name" onChange={this.handleBotNameChange}/>
                </FormGroup>

                <FormGroup controlId="formHorizontalPassword">
                  <ControlLabel>Bot Description</ControlLabel>
                  <FormControl type="text" placeholder="Bot Description" onChange={this.handleBotDescriptionChange} />
                </FormGroup>
                <FormGroup>
                  <Col>
                    <Button type="button" bsStyle="primary" bsSize="small" onClick={this.create.bind(this)}>
                      Create Bot
                    </Button>
                  </Col>
                </FormGroup>
              </Form>
          </Col>
        </Row>
      </Grid>

      
    );
  }
}


export default connect(mapStateToProps)(CreateBotForm);