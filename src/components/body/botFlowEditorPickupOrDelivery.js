import React, { Component } from 'react';
import { Link } from 'react-router'
import { Button ,Grid, Row, Col, Table, } from 'react-bootstrap'

class BotFlowEditorDeliveryOrPickup extends Component{
	render(){
    const bot_id = window.sessionStorage.getItem("bot_id");
    return (
	      <Grid fluid="true">
	          <Row className="rowStyle">
	            <Col md={12} mdOffset={1} >
	              <Table striped bordered >
	                <tbody>
	                  <tr>
	                    <td>Job Name</td>
	                    <td>
	                      <div className="td2Style">
	                        Order-001
	                      </div></td>
	                    <td>
	                      <div className="td2Style">
	                        <Button type="button" bsStyle="primary" bsSize="small"> Save </Button>&nbsp;<Link to="/bot/bot_flow_editor/checkout"><Button type="button" bsStyle="primary" bsSize="small"> Next </Button></Link>
	                      </div>
	                    </td>
	                  </tr>
	                  <tr>
	                    <td>
	                      <div className="td1Style">
	                        Associated Bot
	                      </div>
	                    </td>
	                    <td>{bot_id}</td>
	                    <td>&nbsp;</td>
	                  </tr>
	                </tbody>
	              </Table>
	            </Col>
	          </Row>
	          <Row className="row2Style">
	            <Col md={12} mdOffset={1} >
	              <label className="text"><b><i><font color="blue">Define Delivery Options</font></i></b></label><br/><br/>
	              <label className="text">4.  Delivery  / Pickup Options</label>
	            </Col>
	          </Row>
	          <Row className="row3Style">
	            <Col md={3}>
	              <label className="text responses">To be decided!</label>
	            </Col>
	           
	          </Row>
	      </Grid>
		);
	}
}

export default BotFlowEditorDeliveryOrPickup;