import React, { Component } from 'react'
import { Link } from 'react-router'
import '../../includes/css/styles.css';
import { Button ,Grid, Row, Col, Table, FormGroup, FormControl } from 'react-bootstrap'

const divStyle = {
  'marginTop': '40px',
  'marginLeft': '-300px',
  'float':'left',
}

const div2Style = {
  'float': 'left',
  'marginTop': '180px',
  'marginLeft': '-510px',
  'width': '500px',
}

const td2Style = {
  'width': '400px',
  'marginBottom': '-28px',
}

const td1Style = {
  'width': '121px',
}

const inputStyle = {
  'width': '200px',
}

const btnCopyStyle = {
  'marginTop': '-87px',
  'marginLeft': '210px',
}

const table2Style = {
  'marginTop': '290px',
  'marginLeft': '-380px',
  'text-align': 'center',
}

class EditBot extends Component {
  render() {
    const bot_name = window.sessionStorage.getItem("bot_name");
    const bot_id = window.sessionStorage.getItem("bot_id");
    return (
        <Grid>
          <Row style={divStyle}>
            <Col md={1} mdOffset={1} >
              <Table striped bordered >
                <tbody>
                  <tr>
                    <td>Bot Name</td>
                    <td colSpan="3">{bot_name}</td>
                  </tr>
                  <tr>
                    <td>
                      <div style={td1Style}>
                        Bot ID
                      </div>
                    </td>
                    <td colSpan="3">
                      <div style={td2Style}>
                        <FormGroup controlId="formHorizontalEmail">
                          <FormControl type="text" placeholder="Bot Name" value={bot_id} style={inputStyle} disabled="true"/>
                        </FormGroup>
                       <Button type="button" bsStyle="primary" bsSize="small" style={btnCopyStyle}> Copy</Button>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Col>
          </Row>
          <Row style={div2Style}>
            <label><b><i>Great!  Now define your Bot's job and associated flow.</i></b></label><br/><br/>
            <span>Job Functions / Persona</span>
          </Row>
          <Row style={table2Style}>
            <Col md={1} mdOffset={1} >
              <Table striped bordered >
                <tbody>
                  <tr>
                    <td colSpan="4"><b>Select a job or Persona</b></td>
                  </tr>
                  <tr>
                    <td>
                      <div style={td1Style}>
                        Job 1
                      </div>
                    </td>
                    <td>
                      <div style={td1Style}>
                        Order
                      </div>
                    </td>
                    <td>
                      <div style={td1Style}>
                        Order-001
                      </div>
                    </td>
                    <td>
                      <div style={td1Style}>
                        <Link to="/bot/bot_flow_editor"><Button type="button" bsStyle="primary" bsSize="small">Add</Button></Link>&nbsp;
                        <Button type="button" bsStyle="success" bsSize="small">Edit</Button>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div style={td1Style}>
                        Job 2
                      </div>
                    </td>
                    <td>
                      <div style={td1Style}>
                        Conceirge
                      </div>
                    </td>
                    <td>
                      <div style={td1Style}>
                        FAQ-001
                      </div>
                    </td>
                    <td>
                      <div style={td1Style}>
                        <Button type="button" bsStyle="primary" bsSize="small">Add</Button>&nbsp;
                        <Button type="button" bsStyle="success" bsSize="small">Edit</Button>
                      </div>
                    </td>
                  </tr>
                 
                  <tr>
                    <td>
                      <div style={td1Style}>
                        Job 3
                      </div>
                    </td>
                    <td>
                      <div style={td1Style}>
                        Help
                      </div>
                    </td>
                    <td>
                      <div style={td1Style}>
                        HELP-001
                      </div>
                    </td>
                    <td>
                      <div style={td1Style}>
                        <Button type="button" bsStyle="primary" bsSize="small">Add</Button>&nbsp;
                        <Button type="button" bsStyle="success" bsSize="small">Edit</Button>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Col>
          </Row>
        </Grid>
    );
  }
}

export default EditBot;

