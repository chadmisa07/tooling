import React, { Component } from 'react';
import { Link } from 'react-router';
import { Button ,Grid, Row, Col, Table, FormControl } from 'react-bootstrap'


class BotFlowEditorOrderCount extends Component {
  render() {
    const bot_id = window.sessionStorage.getItem("bot_id");
    return (
      <Grid fluid="true">
          <Row className="rowStyle">
            <Col md={12} mdOffset={1} >
              <Table striped bordered >
                <tbody>
                  <tr>
                    <td>Job Name</td>
                    <td>
                      <div className="td2Style">
                        Order-001
                      </div></td>
                    <td>
                      <div className="td2Style">
                        <Button type="button" bsStyle="primary" bsSize="small"> Save </Button>&nbsp;<Link to="/bot/bot_flow_editor/cart"><Button type="button" bsStyle="primary" bsSize="small"> Next </Button></Link>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div className="td1Style">
                        Associated Bot
                      </div>
                    </td>
                    <td>{bot_id}</td>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
              </Table>
            </Col>
          </Row>
          <Row className="ordeCountRow2Style">
            <Col md={12} mdOffset={1} >
              <label className="text"><b><i><font color="blue">Define Quantity interface</font></i></b></label><br/><br/>
              <label className="text">2. Edit Text for asking how many orders when adding to cart</label>
            </Col>
          </Row>
          <Row className="row4Style">
            <Col md={3}>
              <label className="text responses">Response Text: </label>
            </Col>
            <Col md={4}>
              <FormControl type="text" placeholder="How many would you like?" />
            </Col>
          </Row>
          <Row className="row4Style">
            <Col md={3}>
              <label className="text responses">Menu Selection Text: </label>
            </Col>
            <Col md={4}>
              <FormControl type="text" placeholder="Please choose or enter a number" />
            </Col>
          </Row>
      </Grid>
    );
  }
}
export default BotFlowEditorOrderCount;
