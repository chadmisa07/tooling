import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux'
import { Button ,Grid, Row, Col, Table, FormControl,  Glyphicon, Thumbnail, Modal } from 'react-bootstrap'
import Slider from 'react-slick'
import { addMenu, addButton } from '../../actions/botActions'
import update from 'react-addons-update';


const mapStateToProps = (state) => {
  return {
    menus: state.menu.menus,
    buttons: state.menu.menu_buttons,
  }
}

class BotFlowEditorMenu extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showModal: false,
      menu_button: 2,
      clickable: false,
      current_button : 0,
    }
  }

  close() {
    this.setState({ showModal: false });
    this.setState({clickable: true})
  }

  edit = (event) => {
    this.setState({current_button: event.target.id})
    // this.setState({current_button: })
    this.setState({ showModal: true });
    this.setState({clickable: true})
  }

  open(){
    let num = this.state.menu_button
    console.log("CURRENT " + this.state.menu_button);
    // this.setState({current_button: })
    this.setState({ showModal: true });
    this.setState({clickable: true})
  }

  save(){
    let array = this.props.menus.slice()
    let array_index = this.props.menus[this.state.current_button]
    let title = this.state.title
    let description = this.state.description
    let image_url = this.state.image_url
    let button_text = this.state.button_text
    let id = this.state.menu_button

    console.log("current >> " + this.state.current_button)

    if(this.state.current_button <= 1){
      let obj = {id:id, title: title, description: description, image_url: image_url, button_text: button_text}
      this.props.dispatch(addMenu(array.concat([obj])))
      console.log("CAROUSEL : " + array)
      this.setState({ showModal: false })
      this.setState({clickable: true})
      this.setState({current_button: 0})
    }
    else{
       let obj = {id:id, title: title, description: description, image_url: image_url, button_text: button_text}
        this.props.dispatch({type:"UPDATE_STATE", menu:obj})
    }
  }

  new(){
    this.setState({clickable: false})
    if(this.state.clickable)
    {
      this.setState({menu_button : this.state.menu_button + 1})
      let array = this.props.buttons.slice()
      let id = this.state.menu_button
      let obj = {id: id}
      this.props.dispatch(addButton(array.concat([obj])))
      console.log("BUTTONS : " + array)
      this.open();
    }
  }

  handleTitleChange = (event) => {
      this.setState({ title: event.target.value }, null)
  }

  handleImageUrlChange = (event) => {
      this.setState({ image_url: event.target.value }, null)
  }

  handleDescriptionChange = (event) => {
      this.setState({ description: event.target.value }, null)
  }

  handleButtonTextChange = (event) => {
      this.setState({ button_text: event.target.value }, null)
  }

  render() {

    const bot_id = window.sessionStorage.getItem("bot_id");
    const settings = {
      className: 'center',
      centerMode: true,
      infinite: false,
      centerPadding: '130px',
      speed: 500
    };

    console.log(this.props.menus.length)
    let mappedMenus = this.props.menus.map((menu) => 
      <div key={menu.id}>
        <Thumbnail src={menu.image_url}>
          <FormControl type="text" className="carouselInput" value={menu.title} disabled={true}/><br/>
          <FormControl type="text" className="carouselInput" value={menu.description} disabled={true}/><br/>
          <Button type="button" bsStyle="primary" bsSize="small" > {menu.button_text} </Button>
          </Thumbnail>
      </div> 
    )

    if(this.props.menus.length === 0) mappedMenus = <div></div>

    let mappedButtons = this.props.buttons.map((button) =>
      <div className="row" id={button.id}>
        <Button bsStyle="primary" className="btnMenuStyle" onClick={this.edit.bind(this)} id={button.id}><Glyphicon glyph="list" />&nbsp;&nbsp;Menu Category {button.id} </Button>
      </div>
    )

    return (
      <Grid fluid={true}>
          <Row className="rowStyle">
            <Col md={12} mdOffset={1} >
              <Table striped bordered >
                <tbody>
                  <tr>
                    <td>Job Name</td>
                    <td>
                      <div className="td2Style">
                        Order-001
                      </div></td>
                    <td>
                      <div className="td2Style">
                        <Button type="button" bsStyle="primary" bsSize="small"> Save </Button>&nbsp;<Link to="/bot/bot_flow_editor/order_count"><Button type="button" bsStyle="primary" bsSize="small"> Next </Button></Link>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div className="td1Style">
                        Associated Bot
                      </div>
                    </td>
                    <td>{bot_id}</td>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
              </Table>
            </Col>
          </Row>
          <Row className="row2Style">
            <Col md={12} mdOffset={1} >
              <label className="text"><b><i><font color="blue">Define the menu</font></i></b></label><br/><br/>
              <label className="text">1. Edit Menu</label>
            </Col>
          </Row>
          <Row className="row3Style">
            <Col md={4}>
              <FormControl type="text" placeholder="Enter Text"  className="inputStyle"/><br/>
              <div id="menu_row" className="menuRowStyle">
                {mappedButtons}
                <Button bsStyle="primary" id="addNewMenu" className="addNewMenu btnAddMenuStyle" onClick={this.new.bind(this)}><Glyphicon glyph="plus" />&nbsp;&nbsp;Add New Menu</Button>
              </div>
            </Col>
            <Col md={5}>
              <div className="carouselDiv">
                <Slider {...settings}>
                    {mappedMenus}
                </Slider>
              </div>
            </Col>
          </Row>
          <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
            <Modal.Header closeButton>
              <Modal.Title>Add New Menu</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <center>
                <Thumbnail>
                  <FormControl type="text" className="carouselInput" placeholder="Title" onChange={this.handleTitleChange}/><br/>
                  <FormControl type="text" className="carouselInput" placeholder="Description" onChange={this.handleDescriptionChange}/><br/>
                  <FormControl type="text" className="carouselInput" placeholder="Image Url" onChange={this.handleImageUrlChange}/><br/>
                  <FormControl type="text" className="carouselInput" placeholder="Button Text" onChange={this.handleButtonTextChange}/>
                </Thumbnail>  
              </center>
            </Modal.Body>
            <Modal.Footer>
              <Button bsStyle="primary" onClick={this.save.bind(this)}>Save</Button><Button onClick={this.close.bind(this)}>Close</Button>
            </Modal.Footer>
          </Modal>
      </Grid>
    );
  }
}


export default connect(mapStateToProps)(BotFlowEditorMenu)
