import React, { Component } from 'react';
import { Button ,Grid, Row, Col, Table,  FormControl} from 'react-bootstrap'


class BotFlowEditorCheckout extends Component {
  render() {
    const bot_id = window.sessionStorage.getItem("bot_id");
    return (
      <Grid fluid="true">
          <Row className="rowStyle">
            <Col md={12} mdOffset={1} >
              <Table striped bordered >
                <tbody>
                  <tr>
                    <td>Job Name</td>
                    <td>
                      <div className="td2Style">
                        Order-001
                      </div></td>
                    <td>
                      <div className="td2Style">
                        <Button type="button" bsStyle="primary" bsSize="small"> Save </Button>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div className="td1Style">
                        Associated Bot
                      </div>
                    </td>
                    <td>{bot_id}</td>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
              </Table>
            </Col>
          </Row>
          <Row className="row2Style">
            <Col md={12} mdOffset={1} >
              <label className="text"><b><i><font color="blue">Define Checkout  Options</font></i></b></label><br/><br/>
              <label className="text">5.  Checkout</label>
            </Col>
          </Row>
          <Row className="row4Style">
            <Col md={3}>
              <label className="text responses">Text 1: </label>
            </Col>
            <Col md={4}>
              <FormControl type="text" placeholder="Prceed to Checkout?" />
            </Col>
          </Row>
          <Row className="row4Style">
            <Col md={3}>
              <label className="text responses">Text 2: </label>
            </Col>
            <Col md={4}>
              <FormControl type="text" placeholder="Do you want a receipt?" />
            </Col>
          </Row>
          <Row className="row4Style">
            <Col md={3}>
              <label className="text responses">Text 3: </label>
            </Col>
            <Col md={4}>
              <FormControl type="text" placeholder="Please provide Email" />
            </Col>
          </Row>
      </Grid>
    );
  }
}
export default BotFlowEditorCheckout;
