import React from 'react'


const Footer = () => (

	<div className="footer">
		&copy; Copyright 2016 Chatbot.com
	</div>
)


export default Footer;