import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logout } from '../../../actions/loginActions';
import { Link } from 'react-router';


let welcome_message = "";

class TopBarMenu extends Component{
  logout(e){
    e.preventDefault();
    welcome_message = "Hi, "+ window.sessionStorage.getItem("username");
    this.props.dispatch(logout(sessionStorage.getItem("username"), sessionStorage.getItem("token")));
  }

  render(){
     welcome_message = "Hi, "+ window.sessionStorage.getItem("username");
    return(
      <div>
        <ul className="topnav" id="myTopnav">
          <li><Link to="/main">Chat Bot</Link></li>
        </ul>
        <div className="settings">
          <span className="headerWelcomeMessage"> {welcome_message} </span>
        </div>
      </div>
      )
    }
}


export default connect()(TopBarMenu);