import React from 'react'

const BotFlowEditorSideBarMenu = () => (
    <div className="sidebar-wrapper">
        <ul className="sidebar-nav">
            <li className={sessionStorage.getItem("botFlowEditorLink1")}>1. Menu</li>
            <li className={sessionStorage.getItem("botFlowEditorLink2")}>2. Order Count</li>
            <li className={sessionStorage.getItem("botFlowEditorLink3")}>3. Cart</li>
            <li className={sessionStorage.getItem("botFlowEditorLink4")}>4. Delivery / Pickup</li>
            <li className={sessionStorage.getItem("botFlowEditorLink5")}>5. Checkout</li>
        </ul>
    </div>
);
	
export default BotFlowEditorSideBarMenu;