import React from 'react'

const MainSideBarMenu = () => (
    <div className="sidebar-wrapper">
        <ul className="sidebar-nav">
            <li className={sessionStorage.getItem("createBotLink1")}>1. Create Bots</li>
            <li className={sessionStorage.getItem("createBotLink2")}>2. Edit Bots</li>
            <li className={sessionStorage.getItem("createBotLink3")}>3. Edit Bot Jobs</li>
          </ul>
    </div>
);
	
export default MainSideBarMenu;