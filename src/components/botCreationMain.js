import React, { Component } from 'react';
import TopBarMenu from './commons/header/index';
import MainSideBarMenu from './commons/sidebar/botCreationSidebar';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { logout } from '../actions/loginActions';

const mapStateToProps = (state) => 
{
  return {
    login: state.login,
  }
}

class BotCreationMain extends Component{

	componentWillMount(){
		 // if(typeof(Storage) !== "undefined") {
   //       	if(sessionStorage.getItem("token") === null || this.props.login.access_token === null)
		 // 	{
		 // 		window.localStorage.clear();
		 // 		this.props.dispatch(logout(sessionStorage.getItem("username"), sessionStorage.getItem("token")));
		 // 		console.log("errorr!!! ~~~" + sessionStorage.getItem("token") + "~~~" + this.props.login.access_token);
		 // 		browserHistory.push("/");
		 // 	}
   //       } 
   //       else 
   //       {
   //           if(this.props.login.access_token === null)
   //           {
   //           	this.props.dispatch(logout(sessionStorage.getItem("username"), sessionStorage.getItem("token")));
		 // 		console.log("errorr!!! ~~~" + sessionStorage.getItem("token") + "~~~" + this.props.login.access_token);
		 // 		browserHistory.push("/");
   //           }
   //       }
	}

	render(){ 
		if(this.props.location.pathname === "/bot" || this.props.location.pathname === "/bot/create_bot"){
			sessionStorage.setItem("createBotLink1","createBotLink1")
			sessionStorage.setItem("createBotLink2","")
			sessionStorage.setItem("createBotLink3","")
		}else if(this.props.location.pathname === "/bot/edit_bot"){
			sessionStorage.setItem("createBotLink1","")
			sessionStorage.setItem("createBotLink2","createBotLink2")
			sessionStorage.setItem("createBotLink3","")
		}
		else{
			sessionStorage.setItem("createBotLink1","")
			sessionStorage.setItem("createBotLink2","")
			sessionStorage.setItem("createBotLink3","createBotLink3")
		}
		return(
			<div>
				<TopBarMenu />
				<MainSideBarMenu />
				<div className="main">
	          		{this.props.children}
	        	</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(BotCreationMain);