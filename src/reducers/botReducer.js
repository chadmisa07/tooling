export default function reducer(state={
	menu: [],
	platform: null,
	name: null,
	description: null,
	creating: false,
	success: false,
	failed: false,
	error: null,
}, action){

	switch(action.type)
	{
		case "CREATE_BOT_REQUEST": {
			return {...state, creating: true}
		}
		case "CREATE_BOT_REQUEST_FULFILLED":{
			return {...state, creating: false, success: true, platform: action.payload.platform, name: action.payload.name, description: action.payload.description}
		}
		case "CREATE_BOT_REQUEST_REJECTED":{
			return {...state, creating: false, failed: true ,error: action.payload}
		}
		default: {
			return state;
		}
	}
	
}