export default function reducer(state={
	username: null,
	access_token: null,
	refresh_token: null,
	expires_in: 0,
	fetching: false,
	fetched: false,
	error: null,
}, action){

	switch(action.type)
	{
		case "LOGIN_REQUEST": {
			return {...state, fetching: true, error: null, username: null, access_token: null, refresh_token: null, expires_in: 0}
		}
		case "LOGIN_REQUEST_FULFILLED": {
			return {...state, fetching: false, fetched: true, access_token:action.payload.access_token, refresh_token: action.payload.refresh_token, expires_in: action.payload.expires_in, username: action.username, error: null}
		}
		case "LOGIN_REQUEST_REJECTED": {
			return {...state, fetching: false ,error: action.payload, username: null, access_token: null, refresh_token: null, expires_in: 0 }
		}
		case "LOGOUT": {
			return {...state, username: null, access_token: null, refresh_token: null, expires_in: 0, fetching: false, fetched: false, error: null,}
		}


		default: {
			return {...state}
		}
	}
}