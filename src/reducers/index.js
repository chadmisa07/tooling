import { combineReducers } from 'redux'

import login from './loginReducer'
import createBot from './botReducer'
import menu from './menuReducer'

export default combineReducers({
	login,
	createBot,
	menu,
})