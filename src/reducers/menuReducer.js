import update from 'react-addons-update';

export default function reducer(state={
	image_url : null,
	title : null,
	description: null,
	button_text: null,
	menus: [],
	menu_buttons: [{id : 1}],
	adding: false,
	success: false,
	failed: false,
	error: null,
}, action){

	switch(action.type)
	{
		case "ADD_MENU_CATEGORY": {
			return {...state, adding: true}
		}
		case "ADD_MENU_CATEGORY_FULFILLED":{
			// return {...state, adding: false, success: true, image_url: action.image_url, title: action.title, description: action.description, button_text: action.button_text}
			return {...state, adding: false, success: true, menus:action.menu}
		}
		case "ADD_MENU_CATEGORY_REJECTED":{
			return {...state, adding: false, failed: true ,error: action.payload}
		}

		case "ADD_NEW_BUTTON":{
			return {...state, adding: true}
		}
		case "ADD_NEW_BUTTON_FULFILLED": {
			return {...state, adding: false, success: true, menu_buttons : action.button}
		}
		case "ADD_NEW_BUTTON_REJECTED": {
			return {...state, adding: false, success: false, failed: true ,error: action.payload}
		}

		case "UPDATE_STATE":{
			return update({...state, menus: {1:{$set: action.menu}}})
		}
		default: {
			return state;
		}
	}
	
}