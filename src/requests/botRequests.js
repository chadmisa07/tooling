import axios from 'axios'

export function createBotRequest(platform, name, description, token){

	return function(dispatch)
	{
		dispatch({type: "CREATE_BOT_REQUEST" })
		axios.post("https://immense-everglades-31919.herokuapp.com/api/bot/",
		"name="+name+"&description="+description+"&platform="+platform
		)
		.then(function (response) {
			if (typeof(Storage) !== "undefined") {
    			window.sessionStorage.setItem("bot_name", response.data.name);
				window.sessionStorage.setItem("bot_id", response.data.id)
			}
			dispatch({type: "CREATE_BOT_REQUEST_FULFILLED", payload: response.data})
		})
		.catch(function (error) {
			dispatch({type: "CREATE_BOT_REQUEST_REJECTED", payload: error})
			alert("Sorry, an error occured. Please try again.");
		});
	}
}


// export function addNewMenu(title, description, image_url, button_text)
export function addNewMenu(menu)
{
	return function(dispatch)
	{
		dispatch({type: "ADD_MENU_CATEGORY_FULFILLED", menu: menu})
		// dispatch({type: "ADD_MENU_CATEGORY_FULFILLED", title: title, description: description, image_url: image_url, button_text: button_text})
	}
}


export function addNewButton(button)
{
	return function(dispatch)
	{
		dispatch({type: "ADD_NEW_BUTTON_FULFILLED", button: button})
		// dispatch({type: "ADD_MENU_CATEGORY_FULFILLED", title: title, description: description, image_url: image_url, button_text: button_text})
	}
}


