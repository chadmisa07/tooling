import axios from 'axios';
import { browserHistory } from 'react-router';

export function getToken(username, password){

	return function(dispatch)
	{
		dispatch({type: "LOGIN_REQUEST" })
		axios.post("https://immense-everglades-31919.herokuapp.com/o/token/",
		"grant_type=password&username="+username+"&password="+password, {
			auth: {
		    		username: "VtR0vkISGUl5diJbETiS6qd3hUqeO6oGPRXTQkwm",
		     		password: "zlnM0cYZBYOWcVowfEg5WcLfKq1RV7Ms3kW5ErQWGG289kJm4PAutexPG8Kz0jsFukLBWOQC359eGM16pOXrgqeZWPlacKgT4KTTZHMLWlIIs9RT6PJ1CptByeV7USq9"
	   		}
		})
		.then(function (response) {

			if (typeof(Storage) !== "undefined") {
    			window.sessionStorage.setItem("username", username);
				window.sessionStorage.setItem("token", response.data.access_token);
    		}

			dispatch({type: "LOGIN_REQUEST_FULFILLED", payload: response.data, username: username})
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.access_token;
		})
		.catch(function (error) {
			dispatch({type: "LOGIN_REQUEST_REJECTED", payload: error})
		});
	}
	
}


export function clearToken(username, token)
{
	return function(dispatch)
	{
		dispatch({type: "LOGOUT", token: token, username: username})
		window.sessionStorage.clear();
		browserHistory.push("/");
	}
}